---
title: "Impossible Pancake"
date: 2021-05-28T13:51:15+02:00
draft: false
tags: [glutenfrei, vegan]
---


| Menge | Zutat |
| --- | --- |
|  175 g | Haferflocken |
|  5 g | Leinsamen |
|  2 - 3 TL | Maisstärke |
|  1 TL | Backpulver |
|  1 TL | Natron |
|  1 | Prise Salz |
|  300 ml | Planzendrink (zum Beispiel Hafermilch) |
|  50 g | Mandelmus oder anderes Nussmus |
|  5 | Datteln |
|  3 EL | Wasser |
|  1 EL | Apfelessig |
|  Etwas Rapsöl (zum Braten) |

1. Haferflocken und Leinsamen mit einem Mixer zu Mehl verarbeiten
1. Datteln zusammen mit dem Wasser mixen
1. Mehl und die restlichen Zutaten zu den Datteln hinzugeben
1. 5 Minuten zur Seite stehen und anziehen lassen(Hafer dickt nach)
1. Eine beschichtete Pfanne erhitzen, etwas Rapsöl reingeben und Hitze reduzieren
1. Pro Pancake 1-2 Esslöffel vom Teig in die Pfanne geben(nicht zu dünn)
1. Jede Seite 2-3 Minuten anbraten
1. Auf Teller stapeln damit sie warm bleiben
1. Mit Nussmus, Obst und Schokolade anrichten

[Quelle: Youtube - Impossible Pancakes - von Lecker Lecker Vegan](https://www.youtube.com/watch?v=2e0jvJfcVFs)
