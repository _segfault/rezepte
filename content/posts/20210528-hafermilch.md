---
title: "Hafermilch"
date: 2021-05-28T13:51:15+02:00
draft: false
tags: [Müsli, vegan]
---


| Menge | Zutat |
| --- | --- |
| 55 g | Haferflocken |
| 3-4 | Datteln |
| 1 l | Wasser |


1. Die Datteln über Nacht in Wasser einweichen
1. Die Haferflocken bei höchster Stufe im Mixer zu Mehl mahlen
1. Die Datteln hinzugeben und zerkleinern
1. Das Wasser hinzugeben und wieder mixen
1. Die Milch in ein Gefäß geben und in den Kühlschrank aufbewaren


Die Milch ist im Kühlschrank ca 2-3 Tage haltbar.
Sie fängt an säuerlich zu schmecken und riechen wenn sie schlecht wird.

Beim Lidl gibt es 500g zarte bio Haferflocken für 0,95€.
