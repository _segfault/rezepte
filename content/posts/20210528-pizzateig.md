---
title: "Pizzateig"
date: 2021-05-28T13:51:16+02:00
draft: false
tags: [Pizza]
---


| Menge | Zutat |
| --- | --- |
| 500 g | Weizenmehl |
| 300 ml | lauwarmes Wasser |
| 15 g | Salz |
| 2 g | Hefe |

1. Wasser mit Salz und Hefe vermischen bis sich die Hefe aufgelöst hat
1. Mehl hinzugeben und für 5 Minuten kneten
1. Den Teig nun auf einer Holzplatte mit einem feuchten Küchentuch für 2 Stunden bei Zimmertemperatur gehen lassen
1. Den Teig danach in 4 Stücke teilen und jeweils für 1 Minute kneten
1. Die Teigstücke in einen Behälter legen und den Behälter luftdicht abschließen
1. Den Behälter mit den Teigstücken nun über Nacht in den Kühlschrank geben
1. Am nächsten Tag aus jedem Teigstück eine Pizza formen
1. (optional) Den Pizzateig jetzt noch einmal eine Stunde mit einem feuchten Küchentuch bei Zimmertemperatur gehen lassen

[Quelle: Youtube - Italien Pizza Dough - von ichkocheheute](https://www.youtube.com/watch?v=2Wn2vuGXWZc)
