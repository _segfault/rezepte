---
title: "Pizzasoße"
date: 2021-05-28T13:51:13+02:00
draft: false
tags: [Pizza]
---


| Menge | Zutat |
| --- | --- |
| 250 ml | Tomate(n), passiert |
| 2 EL | Tomatenmark |
| 1 kleine | Zwiebel(n), fein gehackt |
| 1 Zehe | Knoblauch, zerdrückt |
| | Basilikum, Oregano, Rosmarin |
| | Olivenöl, kalt gepresst |
| | Zucker |
| | Meersalz, grobkörnig |
| | Pfeffer, geschrotet |



1. Zwiebeln in heißem Öl glasig anbraten
1. Passierten Tomaten dazu geben und aufkochen lassen
1. Mit den restlichen Zutaten für ca 20-25m leicht köcheln lassen

Schmeckt am besten mit frischen Kräutern und wenn es über Nacht zieht

[Quelle: Chefkoch Pizzasauce von alexandradugas](https://www.chefkoch.de/rezepte/1047221209572594/Pizzasauce.html)
